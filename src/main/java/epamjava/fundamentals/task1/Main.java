package epamjava.fundamentals.task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Enter your name: ");
        String name = scan.nextLine();

        System.out.print("Enter a sequence of numbers divided by comma: ");
        List<Integer> numbers = parseNumbers(scan.nextLine());

        System.out.println("Your name is " + name);
        System.out.println("Sum of the sequence of numbers: " + calculateSum(numbers));
        System.out.println("Multiplication of the sequence of numbers: " + calculateMultiplication(numbers));

        scan.close();
    }

    private static List<Integer> parseNumbers(String numbers) {
        List<Integer> parsedNumbers = new ArrayList<>();
        for (String s : numbers.split("\\s*,\\s*")) {
            parsedNumbers.add(Integer.parseInt(s));
        }
        return parsedNumbers;
    }

    private static int calculateSum(List<Integer> numbers) {
        return numbers.stream().reduce(0, (a, b) -> a + b);
    }

    private static int calculateMultiplication(List<Integer> numbers) {
        return numbers.stream().reduce(1, (a, b) -> a * b);
    }
}
